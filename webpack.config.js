const {resolve} = require('path')
const { VueLoaderPlugin }  = require('vue-loader')
const miniCss = require('mini-css-extract-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
// const {glob} = require('glob');
const terserJs = require('terser-webpack-plugin')
// function makeList(){
//     const list = {

//     }
//     const listPaths = glob.sync("./packages/components/**/index.js")
//     listPaths.forEach(path=>{
//         const entryKey = path.match(/packages\/components\/(.*)\/index.js$/)[1]
//         list[entryKey] = path
//         list[entryKey+'.min'] = path

//     })
//     return list
// }

// const entryList = makeList();
module.exports={
    mode:"none",
    entry: './src/main.js',
    // 模板来源
    output:{
        // 打包后的资源文件
        path:resolve( __dirname ,'./dist'),
        publicPath: './',
        //打包后的资源文件交什么
        // filename:"[name]-[hash:6].umd.js",
        filename:"[name].js",
        library:"blog",
        libraryTarget: "umd",
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: "vue-loader",
              },
          {
            test: /\.less$/,
            use: [miniCss.loader, "css-loader" ,"postcss-loader", "less-loader"],
          },
        ],
    },
    optimization:{
        minimize: true,
        minimizer:[
            new terserJs({
                test:  /\.min\.js$/
            })
        ]
    },
    plugins:[
        new VueLoaderPlugin(),
        new CleanWebpackPlugin(),
        new miniCss({
            filename: "css/[name].css"
        })
    ]
}