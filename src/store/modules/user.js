// /store/modules/exampleStore.js
const state = {
    userInfo: localStorage.getItem('userInfo')?JSON.parse(localStorage.getItem('userInfo')): null
};

const actions = {
  
};

const mutations = {
    editUserInfo(state, newValue) {
        state.userInfo = newValue;
    },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
