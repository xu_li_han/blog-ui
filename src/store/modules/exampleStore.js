// /store/modules/exampleStore.js

const state = {
    example: '举个例子'
};

const actions = {
  
};

const mutations = {
    editExample(state, newValue) {
        state.example = newValue;
    },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
