import axios from 'axios'
// import Vue from 'vue';
// import { Toast } from 'vant';

// Vue.use(Toast);

const defaultHeader = {
    'Content-Type': 'application/json',
    'retry': 5,
    'retryCount': 0
};

const request = function({baseURL = '', timeout = 5000, headers = defaultHeader}) {
    const service = axios.create({
        baseURL,
        timeout,
        headers,
        withCredentials: true
    })

    service.interceptors.request.use(
        config => {
            //config.data = Qs.stringify(config.data)
            //config.headers['Authentication-Admin-Web-Token'] = getToken()
            return config
        },
        error => {
            return Promise.reject(error)
        }
    )

    service.interceptors.response.use(
        response => {
            const {httpCode, msg: resultMessage, result, ret, successful = false} = response.data;

            // 用户登录超时统一处理
            if (httpCode === 401 || ret === '401') {
                window.location.href = '/productList';
                sessionStorage.clear();
                return;
            }

            if (httpCode !== 200 && !successful) {
                // alert(resultMessage)
                console.log(resultMessage)
                return Promise.reject(result);
            }
            return {...response.data};
        },
        error => {
            console.log('err: ' ,error)
            return Promise.reject(new Error('网络超时,请重试'))
        }
    )
    return service;
}

export const apiAjax = new request({baseURL: process.env.NODE_ENV === 'production' ? '' : '/api'});
