  //  * 密码包含 数字,英文,字符中的两种以上,长度6-12 
  export function isvalidPassword(str) {
    const reg = /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$).{6,12}$/
    return reg.test(str)
  }
