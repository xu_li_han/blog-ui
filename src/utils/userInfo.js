import store from '@/store/index.js'

class UserInfo{
    constructor(userInfo = window.localStorage.getItem('userInfo') ){
        this.userInfo = userInfo;
    }
    getInfo(){
        if(this.userInfo){
            return this.userInfo
        }else{
            return null
        }
    }
    setInfo(){
        store.commit("user/editUserInfo", this.userInfo);
        window.localStorage.setItem('TOKEN' , this.userInfo.token)
        return  window.localStorage.setItem('userInfo' , JSON.stringify(this.userInfo))
    }

}

// let USER_INFO = new UserInfo();
export default UserInfo