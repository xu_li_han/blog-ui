const enumList = Object.freeze({
    personalList:{
        dynamic: '动态',
        blogsList: '文章',
        hot: '沸点',
        following: '关注'
    },

    personalListPath:{
        dynamic: '/personalCenter/dynamic',
        blogsList: '/personalCenter/blogsList',
        hot: '/personalCenter/hot',
        following: '/personalCenter/following'
    },

})

export default enumList