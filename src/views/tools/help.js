const enumList = Object.freeze({
    regex: `export const typeOf = function(obj) {
        return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase()}`,
    debounce: `
       export const debounce = (() => {
          let timer = null
          return (callback, wait = 800) => {
            timer&&clearTimeout(timer)
            timer = setTimeout(callback, wait)
          }
        })()
       `,
    throttle: `export const throttle = (() => {
          let last = 0
          return (callback, wait = 800) => {
            let now = +new Date()
            if (now - last > wait) {
              callback()
              last = now
            }
          }
        })()
     `,
     phone:`export const hideMobile = (mobile) => {
          return mobile.replace(/^(\d{3})\d{4}(\d{4})$/, "$1****$2")
        }
    `,
    largeScreen:`export const launchFullscreen = (element) => {
          if (element.requestFullscreen) {
            element.requestFullscreen()
          } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen()
          } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen()
          } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullScreen()
          }
        }
    `,
    closeLarge:`export const launchFullscreen = (element) => {
          if (element.requestFullscreen) {
            element.requestFullscreen()
          } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen()
          } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen()
          } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullScreen()
          }
        }
    `,
    turnCase:`export const turnCase = (str, type) => {
          switch (type) {
            case 1:
              return str.toUpperCase()
            case 2:
              return str.toLowerCase()
            case 3:
              //return str[0].toUpperCase() + str.substr(1).toLowerCase() // substr 已不推荐使用
              return str[0].toUpperCase() + str.substring(1).toLowerCase()
            default:
              return str
          }
        }
    `,
    getSearchParams:`export const getSearchParams = () => {
          const searchPar = new URLSearchParams(window.location.search)
          const paramsObj = {}
          for (const [key, value] of searchPar.entries()) {
            paramsObj[key] = value
          }
          return paramsObj
        }
    `,
    getOSType:`/** 
     * 1: ios
     * 2: android
     * 3: 其它
     */
    export const getOSType=() => {
      let u = navigator.userAgent, app = navigator.appVersion;
      let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1;
      let isIOS = !!u.match(/(i[^;]+;( U;)? CPU.+Mac OS X/);
      if (isIOS) {
        return 1;
      }
      if (isAndroid) {
        return 2;
      }
      return 3;
    }
`




})

const enumName = Object.freeze({
    regex: '校验格式',
    debounce: '防抖',
    throttle: '节流',
    phone:'手机号脱敏',
    largeScreen:'开启全屏',
    closeLarge: '关闭全屏',
    turnCase:"大小写切换",
    getSearchParams:'解析url参数',
    getOSType: '判断手机是Andoird还是IOS'

})

export default {
    enumList,
    enumName
};
