import {apiAjax} from '@/utils/request'

//! 可以在这写接口

export const isExistApi = (params) => {
    return apiAjax({
      url: "/user/isExist",
      method: "get",
      params,
    });
  };

  export const isExistPostApi = (params) => {
    return apiAjax({
      url: "/user/register",
      method: "post",
      params,
    });
  };

// export  function isExistApi(params = {}) {
//     return apiAjax.get(`/user/isExist`,{params: {...params}})
// }

// export function isExistPostApi(params = {}) {
//     return apiAjax.post(`/user/isExist`, {...params})
// }