const files = require.context('./', true, /\.js$/);
// console.log(files.keys())           //["./home/router.js", "./login/router.js"]

 let menus = files.keys().map(fileName => {
  // let obj ={}
  const routerConfig = files(fileName);
  // 因为得到的 fileName 格式是: './gridView.js', 所以这里我们去掉头和尾，只保留真正的文件名
  // const routerName = fileName.replace(/^\.\//, "").replace(/\.\w+$/, "");
  // console.log(routerName);
  // console.log(routerConfig.default)
  return routerConfig.default
})

menus = menus.filter(i=> i)
menus.forEach(i=>{
  if(!i.sort){
    i.sort = 999
  }
})

for(var i=0,len=menus.length;i<len;i++){
  for(var j=i+1;j<len;j++){
    if(menus[i].sort>menus[j].sort){  //将最大的放在最后
        var temp=0;
        temp=menus[i]; 
        menus[i]=menus[j];
        menus[j]=temp;
    }
  }
}

  console.log('%c [ menus ]-15', 'font-size:13px; background:pink; color:#bf2c9f;', menus)

export default {
    menus
}

