const pierceThrough = {
    path: 'pierceThrough',
    name: 'pierceThrough',
    component: () => import('@/views/pierceThrough/index.vue'),
    meta: {
      affix: true,
      title: '花生壳内穿透',
      currentActiveMenu: '/home/pierceThrough'
    },
  }

  export default pierceThrough