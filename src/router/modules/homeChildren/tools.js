const tools = {
    path: 'tools',
    name: 'tools',
    component: () => import('@/views/tools/index.vue'),
    meta: {
      affix: true,
      title: '工具',
      currentActiveMenu: '/home/tools'
    },
  }

  export default tools