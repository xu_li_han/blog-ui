
const router = {
    path: '/personalCenter',
    name: 'personalCenter',
    component: () => import( '@/views/personalCenter/index.vue'),
    children: [
      {
        path: 'dynamic',
        name: 'dynamic',
        component: () => import('@/views/dynamic/index.vue'),
        meta: {
          affix: true,
          title: '动态',
          currentActiveMenu: '/personalCenter/dynamic'
        },
      },
      {
        path: 'blogsList',
        name: 'blogsList',
        component: () => import('@/views/blogsList/index.vue'),
        meta: {
          affix: true,
          title: '文章',
          currentActiveMenu: '/personalCenter/blogsList'
        },
      },
      {
        path: 'hot',
        name: 'hot',
        component: () => import('@/views/hot/index.vue'),
        meta: {
          affix: true,
          title: '沸点',
          currentActiveMenu: '/personalCenter/hot'
        },
      },
      {
        path: 'following',
        name: 'following',
        component: () => import('@/views/following/index.vue'),
        meta: {
          affix: true,
          title: '关注',
          currentActiveMenu: '/personalCenter/following'
        },
      },
    ]
  }

  export default router

