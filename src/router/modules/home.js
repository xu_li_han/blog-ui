import Home from '@/views/Home.vue'

const files = require.context('./homeChildren', true, /\.js$/);
 const arr = files.keys().map(fileName => {
  // let obj ={}
  const routerConfig = files(fileName);
  return routerConfig.default
})
console.log('%c [ arr ]-5', 'font-size:13px; background:pink; color:#bf2c9f;', arr)


const router =   {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: 'homeDetail',
        name: 'homeDetail',
        component: () => import('@/views/homeDetail/index.vue'),
        meta: {
          affix: true,
          title: '首页',
          currentActiveMenu: '/home/homeDetail'
        },
      },
      {
        path: 'aboutMe',
        name: 'aboutMe',
        component: () => import('@/views/aboutMe/index.vue'),
        meta: {
          affix: true,
          title: '首页',
          currentActiveMenu: '/home/aboutMe'
        },
      },
      {
        path: 'game',
        name: 'game',
        component: () => import('@/views/game/index.vue'),
        meta: {
          affix: true,
          title: '首页',
          currentActiveMenu: '/home/game'
        },
      },
    ]
  }

  router.children = [...router.children , ...arr]

  export default router

