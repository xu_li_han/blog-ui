
const router = {
    path: '/writeBlog',
    name: 'writeBlog',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/writeBlog/index.vue')
  }

  export default router

