
import { loginGuard } from './loginGuard';
import {firstTimeGuard} from './firstTimeGuard'
import { personalCenterGuard } from './personalCenterGuard'


export function createGuard(router) {
    loginGuard(router);
    firstTimeGuard(router);
    personalCenterGuard(router)
}