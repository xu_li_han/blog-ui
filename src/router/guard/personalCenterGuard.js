
export function personalCenterGuard(router) {
    router.beforeEach(async (to, from, next) => {
        if (to.path.includes('personalCenter') && from.query.id && !to.query.id) {
            let toQuery = JSON.parse(JSON.stringify(to.query))
            toQuery.id = from.query.id
            next({
                path: to.path,
                query: toQuery,
            })
        } else {
            next()
        }
    });
}

export default personalCenterGuard