import Vue from 'vue'
import VueRouter from 'vue-router'
import {createGuard} from './guard/index'

Vue.use(VueRouter)
const files = require.context('./modules', true, /\.js$/);
// console.log(files.keys())           //["./home/router.js", "./login/router.js"]

 const routes = files.keys().map(fileName => {
  // let obj ={}
  const routerConfig = files(fileName);
  // 因为得到的 fileName 格式是: './gridView.js', 所以这里我们去掉头和尾，只保留真正的文件名
  // const routerName = fileName.replace(/^\.\//, "").replace(/\.\w+$/, "");
  // console.log(routerName);
  // console.log(routerConfig.default)
  return routerConfig.default
})




// const routes = [
//   {
//     path: '/',
//     name: 'Home',
//     component: Home
//   },
//   {
//     path: '/about',
//     name: 'About',
//     // route level code-splitting
//     // this generates a separate chunk (about.[hash].js) for this route
//     // which is lazy-loaded when the route is visited.
//     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
//   }
// ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// 导航首位
createGuard(router)

export default router
