import serviceAxios from "../index";

//! 可以在这写接口

export const fansApi = (params) => {
    return serviceAxios({
        url: "/api/user-relation/fans",
        method: "get",
        params,
    });
};

export const followersApi = (params) => {
    return serviceAxios({
        url: "/api/user-relation/followers",
        method: "get",
        params,
    });
};

export const joinGameApi = (params) => {
    return serviceAxios({
        url: "/api/user-relation/followers",
        method: "get",
        params,
    });
};

export function isFollowApi(params = {}) {
    return serviceAxios.post(`/api/user-relation/isFollow`, {...params})
}

