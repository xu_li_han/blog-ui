import serviceAxios from "../index";

//! 可以在这写接口

export const isExistApi = (params) => {
  return serviceAxios({
    url: "/api/user/isExist",
    method: "get",
    params,
  });
};

export const userInfoApi = (params) => {
  return serviceAxios({
    url: "/api/user/userInfo",
    method: "get",
    params,
  });
};

export function isExistPostApi(params = {}) {
  return serviceAxios.post(`api/user/isExist`, {...params})
}

export const registerApi = (params) => {
  return serviceAxios.post(`api/user/register`, {...params})
};

export const loginApi = (params) => {
  return serviceAxios.post(`api/user/login`, {...params})
};



