import serviceAxios from "../index";

//! 可以在这写接口

export const gameListApi = (params) => {
  return serviceAxios({
    url: "/api/game/list",
    method: "get",
    params,
  });
};

export const joinGameApi = (params) => {
    return serviceAxios({
      url: "/api/game/list/join",
      method: "get",
      params,
    });
  };



export function isDelPostApi(params = {}) {
    return serviceAxios.post(`api/game/list/del`, {...params})
  }

  export function addPlayerApi(params = {}) {
    return serviceAxios.post(`api/game/list/add`, {...params})
  }

  export function editScoreApi(params = {}) {
    return serviceAxios.post(`api/game/list/gameover`, {...params})
  }