import serviceAxios from "../index";

//! 可以在这写接口

export const blogsList = (params) => {
  return serviceAxios({
    url: "/api/blogs/list",
    method: "get",
    params,
  });
};






