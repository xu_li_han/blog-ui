const serverConfig = {
    baseURL: process.env.NODE_ENV=== 'production'?"https://640gh54102.zicp.fun":"http://localhost:3090", // 请求基础地址,可根据环境自定义
    // baseURL: "https://640gh54102.zicp.fun", // 请求基础地址,可根据环境自定义
    // process.env.NODE_ENV
    useTokenAuthorization: true, // 是否开启 token 认证
  };

  export default serverConfig;