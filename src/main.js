import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { ceshi } from './global'
Vue.use(ElementUI);

import VueCodemirror from 'vue-codemirror'
import 'codemirror/lib/codemirror.css'
import VueMarkdown from 'vue-markdown'

Vue.prototype.ceshi = ceshi


Vue.use(VueCodemirror, /* { 
  options: { theme: 'base16-dark', ... },
  events: ['scroll', ...]
} */)

Vue.config.productionTip = false


new Vue({
  router,
  store,
  components:{
    'vue-markdown': VueMarkdown
  },
  render: h => h(App)
}).$mount('#app')
