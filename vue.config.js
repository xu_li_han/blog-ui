module.exports = {
    configureWebpack:{
        resolve:{
            // 给路径起别名
            alias:{
                'assets': '@/assets',
                'common': '@/common',
                'components': '@/components',
                'network': '@/network',
                'views': '@/views'
            }
        }
    },
  

    
    devServer: {
        // proxy:{
        //     '/api':{
        //         // 跨域的服务器地址
        //         // target: 'http://zylwt.com',
        //         target: 'http://localhost:3000',
        //         // 是否允许跨域
        //         changeOrigin: true,
        //         // 替换掉请求路径的/json_demo“”
        //         pathRewrite:{'^/api': "api"}
        //     },
        // },
        proxy:{
            '/api':{
                // 跨域的服务器地址
                // target: 'http://zylwt.com',
                target: process.env.NODE_ENV=== 'production'?"https://640gh54102.zicp.fun":"http://localhost:3090",
                // 是否允许跨域
                changeOrigin: true,
                // 替换掉请求路径的/json_demo“”
                pathRewrite:{'^/api': "api"}
            },
        },
        hot: true,
        port: 3020,
        //   proxy: {
        //     '/api': {
        //       target: 'http://test.jianyiyun.com',
        //     },
        //   }
    }
}